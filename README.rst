bugzilla-docstrings
===================

.. image:: https://gitlab.com/jdupuy/bugzilla_docstrings/badges/master/pipeline.svg
    :target: https://gitlab.com/jdupuy/bugzilla_docstrings/commits/master
    :alt: pipeline status

.. image:: https://gitlab.com/jdupuy/bugzilla_docstrings/badges/master/coverage.svg
    :target: https://gitlab.com/jdupuy/bugzilla_docstrings/commits/master
    :alt: coverage report

.. image:: https://img.shields.io/pypi/v/bugzilla-docstrings.svg
    :target: https://pypi.python.org/pypi/bugzilla-docstrings
    :alt: Version

.. image:: https://img.shields.io/pypi/pyversions/bugzilla-docstrings.svg
    :target: https://pypi.python.org/pypi/bugzilla-docstrings
    :alt: Supported Python Versions

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/ambv/black
    :alt: Code style: black

Description
-----------
Library for reading Bugzilla docstrings used in Insights and CFME QE. Includes a flake8 extension for validating the docstrings.
This is a fork of polarion-docstrings

Install
-------
To install the package to your virtualenv, run

.. code-block::

    pip install bugzilla-docstrings

or install it from cloned directory

.. code-block::

    pip install -e .

Package on PyPI <https://pypi.python.org/pypi/bugzilla-docstrings>
